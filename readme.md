1. Як можна оголосити змінну у Javascript?
	Є три способи (зараз використовують лише два):
		var  - не рекомендується використовувати, застарілий спосіб і наступні - зручніші;
		let -  значення можна буде змінювати;
		const - значення змінити не можна буде;

2. У чому різниця між функцією prompt та функцією confirm?
	Функція confirm -  виводить модальне вікно з повідомленням та має дві кнопки: "ОК" та "Скасувати",
	та вертає true / false відповідно до того, на що натиснув користувач, а функція prompt має ще також
	поле для вводу і повертає стрічку, з введиними даними або пусту, або null - якщо скасували.

3. Що таке неявне перетворення типів? Наведіть один приклад.
	Неявне перетворення типів, коли не ми вручну прописуємо перетворення типів, а це відбувається автоматично внаслідок якихось операторів/функцій тощо.
	Приклад:
	alert("6" / 3); //2
	Тут оператор ділення / приведе стрічку до числа і виконає ділення.